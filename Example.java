package base;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.Vector;

import static base.Wall.WallOfInt;

public class Example extends Application {

    private double width = 800;
    private double height = 600;
    private double speed = 500;
    private double dirX = 2;
    private double dirY = 1;
    private double oldX = 50;
    private double oldY = 50;
    private double nextX = 50;
    private double nextY = 50;
    private Circle circle = new Circle();

    public Wall computeNextWall(Circle circle, double x, double y, double dirX, double dirY)
    {
        System.out.println("G next wall");

        assert(x>=0 && x<= width);
        assert(y>=0 && y<= height);

        Vector candidates = new Vector(2);
        double xRatio = 0;
        double yRatio = 0;

        if (dirX > 0) //hits the right wall
        {
            candidates.addElement(1);
            xRatio = (width-x-circle.getRadius())/dirX;
        }
        else if (dirX < 0) //hits the left wall
        {
            candidates.addElement(3);
            xRatio = (x-circle.getRadius())/dirX;
        }
        if (dirY > 0) //hits the lower wall
        {
            candidates.addElement(2);
            yRatio = (height-y-circle.getRadius())/dirY;
        }
        else if (dirY < 0) //hits the upper wall
        {
            candidates.addElement(0);
            yRatio = (y-circle.getRadius())/dirY;
        }
        if (candidates.size() == 1)
        {
            return WallOfInt((int) candidates.elementAt(0));
        }
        else
        {
            if (Math.abs(xRatio) <= Math.abs(yRatio))
                return WallOfInt((int) candidates.elementAt(0));
            else
                return WallOfInt((int) candidates.elementAt(1));
        }
    }

    private Vector computeWallImpact(Circle circle, double x, double y, double dirX, double dirY, Wall wall)
    {
        System.out.println("H wall impact");

        assert(x>=0 && x<= width);
        assert(y>=0 && y<= height);

        double deltaX = 0;
        double deltaY = 0;

        Vector informations = new Vector(3);
        switch (wall)
        {
            case UPPER:
                deltaY = y - circle.getRadius();
                deltaX = deltaY * dirX/Math.abs(dirY);
                informations.addElement(x+deltaX);
                informations.addElement(circle.getRadius());
                break;
            case RIGHT:
                deltaX = width-x-circle.getRadius();
                deltaY = deltaX*dirY/Math.abs(dirX);
                informations.addElement(width-circle.getRadius());
                informations.addElement(y+deltaY);
                break;
            case LOWER:
                deltaY = height-y-circle.getRadius();
                deltaX = deltaY*dirX/Math.abs(dirY);
                informations.addElement(x+deltaX);
                informations.addElement(height-circle.getRadius());
                break;
            case LEFT:
                deltaX = x-circle.getRadius();
                deltaY = deltaX*dirY/Math.abs(dirX);
                informations.addElement(circle.getRadius());
                informations.addElement(y+deltaY);
                break;
        }
        System.out.println("-------------- x");
        System.out.println(x);
        System.out.println("-------------- delta x");
        System.out.println(deltaX);
        System.out.println("-------------- y");
        System.out.println(y);
        System.out.println("-------------- delta y");
        System.out.println(deltaY);

        double distance = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
        informations.addElement(distance/speed);
        return informations;
    }

    public class MyAnimationTimer extends AnimationTimer
    {
        private long startTime = -1;

        @Override
        public void handle(long timestamp)
        {
            if (startTime==-1)
                startTime = timestamp;

            long totalElapsedNanoseconds = timestamp - startTime;
            long n = totalElapsedNanoseconds/(1000000000);
            if (n%2 == 0)
                circle.setFill(Color.RED);
            else
                circle.setFill(Color.GREEN);
        }
    }

    @Override
    public void start(Stage primary) throws Exception {
        primary.setTitle("My first TimeLine");

        int radius = 50;

        Group root = new Group();
        Scene scene = new Scene(root, width, height);

        scene.setFill(Color.BLUE);
        primary.setScene(scene);


        circle.setRadius(radius);
        circle.setCenterX(0);
        oldX = radius;
        oldY = radius;
        circle.setCenterY(0);
        circle.setFill(Color.YELLOW);

        root.getChildren().add(circle);

        Timeline timeline = new Timeline();

        EventHandler<ActionEvent> next = event -> {
            System.out.println("F next");

            Wall wall = computeNextWall(circle, oldX, oldY, dirX, dirY);
            System.out.println("-------------- wall ");
            System.out.println(wall);

            Vector informations = computeWallImpact(circle, oldX, oldY, dirX, dirY, wall);
            computeNextDir(wall);

            timeline.getKeyFrames().clear();
            //System.out.println(informations.get(0));
            System.out.println("-------------- x final ");
            System.out.println(informations.get(0));
            System.out.println("-------------- y final ");
            System.out.println(informations.get(1));
            System.out.println("-------------- futur dirX ");
            System.out.println(dirX);
            System.out.println("-------------- futur dirY ");
            System.out.println(dirY);
            nextX = (double) informations.get(0);
            nextY = (double) informations.get(1);

            timeline.getKeyFrames().addAll(
                    new KeyFrame(new Duration(1000*(double) informations.get(2)), new KeyValue(circle.translateXProperty(), nextX)),
                    new KeyFrame(new Duration(1000*(double) informations.get(2)), new KeyValue(circle.translateYProperty(), nextY)));
            timeline.play();

            oldX = nextX;
            oldY = nextY;
        };

        timeline.setOnFinished(next);
        timeline.getKeyFrames().add(new KeyFrame(Duration.ZERO, new KeyValue(circle.fillProperty(), Color.YELLOW)));

        primary.show();
        timeline.play();

        AnimationTimer timer = new MyAnimationTimer();
        timer.start();
    }

    private void computeNextDir(Wall wall)
    {
        switch (wall)
        {
            case UPPER: dirY = -dirY;
                break;
            case RIGHT: dirX = -dirX;
                break;
            case LOWER: dirY = -dirY;
                break;
            case LEFT: dirX = -dirX;
                break;
            case ERROR:
                break;
        }
    }

    public static void main(String args[])
    {
        launch(args);
    }


}
