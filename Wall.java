package base;

enum Wall
{
    UPPER, RIGHT, LOWER, LEFT, ERROR;

    public static Wall WallOfInt(int a)
    {
        assert(a>=0 && a<=3);
        switch (a)
        {
            case 0: return UPPER;
            case 1: return RIGHT;
            case 2: return LOWER;
            case 3: return LEFT;
        }
        return ERROR;
    }


};

