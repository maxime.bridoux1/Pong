package base;

import base.display.Ball;
import base.display.Field;
import base.display.Score;
import base.display.maps.ScreenPowerSaver;
import base.display.obstacle.Obstacle;
import base.display.racket.HumanRacket;
import base.display.racket.IARacket;
import com.sun.javafx.tk.FontLoader;
import com.sun.javafx.tk.Toolkit;
import data.Point;
import data.Segment;
import data.Vector;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;

public class Engine extends Application
{
    private double _width = 800;
    private double _height = 600;
    private double _speed = 0.5;
    private double nextX = 0;
    private double nextY = 0;
    private double _radius = 10;
    private Field _map = new ScreenPowerSaver(_width, _height);
    private ArrayList<Ball> _balls = new ArrayList<>(1);
    private ArrayList<Timeline> _timelines = new ArrayList<>(1);

    @Override
    public void start(Stage stage) {
        stage.setTitle("Pong");

        Ball newBall = new Ball(_radius, _radius, 1, 1, _radius, Color.BLACK);
        addNewBall(newBall);

        Timer timer = new Timer(_map);

        _map.setStageScene(stage);

        HumanRacket racket1 = new HumanRacket(new Point(780, 300), 20, 200, 0, _height);
        _map.addRacket(racket1);

        IARacket racket2 = new IARacket(new Point(20, 300), 20, 200, 0, _height);
        _map.addRacket(racket2);

        for (int i = 0; i < _balls.size(); i++)
        {
            _timelines.add(new Timeline());
            Timeline timeline = _timelines.get(i);
            Ball ball = _balls.get(i);
            timeline.getKeyFrames().addAll(
                    new KeyFrame(Duration.ZERO, new KeyValue(ball.getCircle().translateXProperty(), ball.getRadius())),
                    new KeyFrame(Duration.ZERO, new KeyValue(ball.getCircle().translateYProperty(), ball.getRadius())));

            timeline.setOnFinished(generateEventHandler(ball, timeline, _map));
        }

        for (int i = 0; i < _balls.size(); i++)
            _timelines.get(i).play();

        timer.start();
        stage.show();
    }

    public static void main(String args[])
    {
        launch(args);
    }

    /**
     * Changes the ball's direction.
     * @param ball the ball
     * @param p the point of impact
     * @param s the impacted segment
     */
    private void bounce(Ball ball, Point p, Segment s)
    {
        //TODO finir tous les cas
        if (s.getX1()==s.getX2())
        {
            ball.setDirection(new Vector(-ball.getDirection().getDirX(), ball.getDirection().getDirY()));
        }
        if (s.getY1() == s.getY2())
        {
            ball.setDirection(new Vector(ball.getDirection().getDirX(), -ball.getDirection().getDirY()));
        }
        ball.setLocation(p);
    }

    /**
     * Makes the ball's interaction with an obstacle.
     * @param o the obstacle
     * @param ball the ball
     * @return true iif there is an interaction
     */
    private boolean interact(Obstacle o, Ball ball)
    {
        if(o.isWinningWall())
        {
            replaceBall(ball);
            addPoint(o.getWinningPlayer());
            return true;
        }
        return false;
    }

    /**
     * Adds a point to a player.
     * @param player the player
     */
    private void addPoint(int player)
    {
        try
        {
            _map.addPoint(player);
        }
        catch (WinException win)
        {
            showWinner(win.getWinningPlayer());
            clearMap();
        }
    }

    /**
     * Show a winning text
     * @param player the winning player
     */
    private void showWinner(int player)
    {
        Label label = new Label("Player " + (player + 1) + " won!");
        label.setFont(new Font("Arial", 30));
        FontLoader fontLoader = Toolkit.getToolkit().getFontLoader();
        double textWidth = fontLoader.computeStringWidth(label.getText(), label.getFont());
        label.setTranslateX((_width-textWidth)/2);
        label.setTranslateY(_height/6);
        label.setTextFill(Color.RED);
        _map.addLabelToScreen(label);
    }

    /**
     * Removes all balls and their timelines from the map.
     */
    private void clearMap()
    {
        for (int i = 0; i < _timelines.size(); i++)
        {
            _timelines.get(i).setOnFinished(event -> {});
            _timelines.get(i).stop();
            _map.removeBallFromScreen(_balls.get(i));
        }
        _timelines.clear();
        _balls.clear();
    }

    /**
     * Replaces a ball with a new one.
     * @param ball the ball to replace
     */
    private void replaceBall(Ball ball)
    {
        int index = _balls.indexOf(ball);

        _timelines.get(index).setOnFinished(event -> {});
        _timelines.get(index).stop();

        Ball newBall = new Ball(_radius, _radius, 1, 1, _radius, Color.BLACK);
        Timeline newTimeline = new Timeline();
        newTimeline.setOnFinished(generateEventHandler(newBall, newTimeline, _map));

        _map.removeBallFromScreen(ball);
        _balls.set(index, newBall);
        _timelines.set(index, newTimeline);
        _map.addBallToScreen(newBall);
        newTimeline.play();
    }

    /**
     * Adds a new ball to the map
     * @param ball the ball to add
     */
    private void addNewBall(Ball ball)
    {
        _balls.add(ball);
        _map.addBallToScreen(ball);
    }

    /**
     * Generates a timeline continuation.
     * @param ball a ball
     * @param timeline the ball's timeline
     * @param map the map
     * @return the event handler to call after the timeline stops
     */
    private EventHandler<ActionEvent> generateEventHandler(Ball ball, Timeline timeline, Field map)
    {
        return (event) -> {
            if (!interact(ball.getCurrentObstacle(),ball))
            {
                Collision next_collision = new Collision(ball, map);

                timeline.getKeyFrames().clear();

                nextX = next_collision.getCoodinates().getX();
                nextY = next_collision.getCoodinates().getY();
                double duration = next_collision.getDistance()/_speed;

                timeline.getKeyFrames().addAll(
                        new KeyFrame(new Duration(duration), new KeyValue(ball.getCircle().translateXProperty(), nextX)),
                        new KeyFrame(new Duration(duration), new KeyValue(ball.getCircle().translateYProperty(), nextY)));

                ball.setCurrentObstacle(next_collision.getObstacle());
                bounce(ball, next_collision.getCoodinates(), next_collision.getSegment());

                timeline.play();
            }
        };
    }
}
