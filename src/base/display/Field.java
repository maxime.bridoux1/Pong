package base.display;

import base.WinException;
import base.display.obstacle.Obstacle;
import base.display.obstacle.Wall;
import base.display.racket.AbstractRacket;
import base.display.racket.IARacket;
import data.Point;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.util.BitSet;
import java.util.Vector;

/**
 * Container for graphical objets.
 */
public abstract class Field
{
    protected Group _root = new Group();
    protected Scene _scene;

    /**
     * The obstacles on the map.
     */
    protected Vector<Obstacle> _obstacle = new Vector<>();

    /**
     * The IA rackets on the map.
     */
    protected Vector<IARacket> _IArackets = new Vector<>();

    /***
     * The number of players the map handles.
     */
    protected int _playerNumber;

    /**
     * The current score.
     */
    protected Score _score;

    public Scene getScene()
    {
        assert(_scene != null);
        return _scene;
    }

    /**
     * Removes a particular ball from the map.
     * @param ball the ball to remove
     */
    public void removeBallFromScreen(Ball ball)
    {
        _root.getChildren().remove(ball.getCircle());
    }

    /**
     * Adds a ball to the map
     * @param ball the ball to add
     */
    public void addBallToScreen(Ball ball)
    {
        _root.getChildren().add(ball.getCircle());
    }

    /**
     * Puts this map on stage.
     * @param stage the stage to view the map
     */
    public void setStageScene(Stage stage)
    {
        stage.setScene(getScene());
    }

    /**
     * Adds an obstacle on the map
     * @param obstacle the obstacle to add
     */
    public void addObstacle(Obstacle obstacle)
    {
        _obstacle.add(obstacle);
        _root.getChildren().add(obstacle.getShape());
    }

    /**
     * Add a racket on the map
     * @param racket the racket to add
     */
    public void addRacket(AbstractRacket racket)
    {
        if (racket.isIA())
            _IArackets.addElement((IARacket) racket);
        _playerNumber += 1;
        racket.setUpControls(_scene);
        addObstacle(racket);
    }

    /**
     * Getter.
     * @return the vector of obstacles on the map
     */
    public Vector<Obstacle> getObstacles()
    {
        return _obstacle;
    }

    /**
     * Generates a rectangle (ABCD) made of walls for the map
     * @param p1 the first point (A)
     * @param p3 the second point (C)
     */
    public void generateWallRect(Point p1, Point p3)
    {
        assert p1 != null;
        assert p3 != null;
        Point p2 = new Point(p3.getX(), p1.getY());
        Point p4 = new Point(p1.getX(), p3.getY());
        addObstacle(new Wall(p1, p2));
        addObstacle(new Wall(p2, p3));
        addObstacle(new Wall(p3, p4));
        addObstacle(new Wall(p4, p1));
    }

    /**
     * Getter.
     * @return the vector of IA rackets
     */
    public Vector<IARacket> getIArackets()
    {
        return _IArackets;
    }

    /**
     * Adds the score view to the stage.
     */
    public void addScore()
    {
        _root.getChildren().add(_score.getLabel());
    }

    /**
     * Adds a point for a player.
     * @param player
     * @throws WinException
     */
    public void addPoint(int player) throws WinException {
        assert (player < _playerNumber);
        _score.addPoint(player);
    }

    /**
     * Adds a label wiew to the stage.
     * @param label the label to view
     */
    public void addLabelToScreen(Label label)
    {
        _root.getChildren().add(label);
    }
}
