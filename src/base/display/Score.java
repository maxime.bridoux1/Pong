package base.display;

import base.WinException;
import javafx.scene.control.Label;

import java.util.ArrayList;

/**
 * Keeps the number of points.
 * Triggers a WinException when a player wins.
 */
public class Score
{
    /**
     * The array of points.
     */
    private ArrayList<Integer> _score;

    /**
     * The on-screen text.
     */
    private Label _label;

    /**
     * The score limit.
     */
    private int _scoreLimit;

    public Score(int playerNumber, int scoreLimit)
    {
        _scoreLimit = scoreLimit;
        _score = new ArrayList<>(playerNumber);
        for (int i = 0; i < playerNumber; i++)
            _score.add(0);

        _label = new Label("Score");
        actualiseScore();
    }

    /**
     * Changes the label to match the current score.
     */
    public void actualiseScore()
    {
        String scoreText = "Score :";
        for (int i = 0; i < _score.size(); i++)
        {
            scoreText = scoreText.concat(" ").concat(_score.get(i).toString());
        }
        _label.setText(scoreText);
    }

    /**
     * Checks if a player wins
     * @throws WinException if a player reaches the point limit
     */
    public void checkScore() throws WinException
    {
        for (int i = 0; i < _score.size(); i++)
        {
            if (_score.get(i) >= _scoreLimit)
            {
                throw new WinException(i);
            }
        }
    }

    /**
     * Adds a point to a player
     * @param player the player that wins a point
     * @throws WinException if the player reaches the point limit
     */
    public void addPoint(int player) throws WinException
    {
        assert(player < _score.size());
        _score.set(player, _score.get(player) + 1);
        actualiseScore();
        checkScore();
    }

    /**
     * Getter.
     * @return the text label
     */
    public Label getLabel()
    {
        return _label;
    }
}
