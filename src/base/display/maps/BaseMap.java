package base.display.maps;

import base.display.Field;

/**
 * Empty map.
 * @deprecated since other maps exist
 * @see ScreenPowerSaver
 */
public class BaseMap extends Field
{
}
