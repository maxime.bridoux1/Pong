package base.display.obstacle;

import base.display.Ball;
import data.Segment;
import javafx.scene.shape.Shape;

import java.util.Vector;


/**
 * Container for all objets with collisions with Ball.
 */
public abstract class Obstacle
{
    /**
     * The list of segments the obstacle is made of.
     */
    protected Vector<Segment> _segments;

    /**
     * The shape of the obstacle.
     */
    protected Shape _shape;

    public Obstacle()
    {
        _segments = new Vector<>();
        addSegment(new Segment (0, 0, 0,0 ));
    }

    /**
     * Getter.
     * @return vector of segments that make the obstacle
     */
    public Vector<Segment> getSegments()
    {
        return _segments;
    }

    /**
     * Adds a segment to the segments' vector.
     * @param s the segment to add
     */
    public void addSegment(Segment s)
    {
        assert (_segments != null);
        _segments.addElement(s);
    }

    /**
     * Virtual method to compute an obstacle's Shape.
     */
    public abstract void createShape();


    /**
     * Getter.
     * @return the obstacle's shape
     */
    public Shape getShape() {
        return _shape;
    }

    /**
     * Getter.
     * @return if hitting the obstacle makes a player win.
     */
    public boolean isWinningWall()
    {
        return false;
    }

    /**
     * Getter.
     * @return which player wins if the obstacle if winning.
     */
    public int getWinningPlayer()
    {
        return -1;
    }
}