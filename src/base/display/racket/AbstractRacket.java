package base.display.racket;

import base.display.obstacle.MovingObstacle;
import data.Point;

import data.Segment;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

import java.applet.Applet;

/**
 * Container for rackets objects.
 */
public abstract class AbstractRacket extends MovingObstacle
{
    /**
     * The center of the racket.
     */
    protected Point _center;

    /**
     * The width of the racket.
     */
    protected double _width;

    /**
     * The height of the racket.
     */
    protected double _height;

    /**
     * The shape of the racket.
     */
    protected Rectangle _shape;

    /**
     * The timeline of the racket.
     */
    protected Timeline _timeline;

    /**
     * The default speed of the racket.
     */
    protected double _speed = 0.01;

    /**
     * If the racket is IA controlled.
     */
    protected boolean _IA;

    public AbstractRacket()
    {
        _timeline = new Timeline();
        _timeline.setAutoReverse(false);
        _timeline.setOnFinished(event -> {
        });
    }

    /**
     * Creates a black rectangle based shape.
     */
    @Override
    public void createShape()
    {
        _shape = new Rectangle(_center.getX()-_width/2,_center.getY()-_height/2,_width,_height);
        setPosition(_center);
        _shape.setFill(Color.BLACK);
    }

    /**
     * Inits the racket.
     */
    public void setUpRacket()
    {
        generateSegments();
        createShape();
    }

    /**
     * Moves the racket relatively to its position
     * @param deltaX the x difference
     * @param deltaY the y difference
     */
    public void move(double deltaX, double deltaY)
    {
        assert (_shape != null);
        _timeline.getKeyFrames().clear();
        _timeline.getKeyFrames().addAll(new KeyFrame(Duration.ZERO,
                new KeyValue(getShape().translateXProperty(), getShape().getTranslateX()+deltaX)),
                new KeyFrame(Duration.ZERO,
                new KeyValue(getShape().translateYProperty(), getShape().getTranslateY()+deltaY)));
        setPosition(_center.add(new Point(deltaX, deltaY)));
    }

    /**
     * Moves the racket to a particular position
     * @param p
     */
    public void moveTo(Point p)
    {
        assert (_shape != null);
        assert (_speed != 0);
        double deltaX = p.getX() - _center.getX();
        double deltaY = p.getY() - _center.getY();
        _timeline.getKeyFrames().clear();
        _timeline.getKeyFrames().addAll(new KeyFrame(new Duration(deltaX/_speed),
                        new KeyValue(getShape().translateXProperty(), getShape().getTranslateX()+deltaX)),
                new KeyFrame(new Duration(deltaY/_speed),
                        new KeyValue(getShape().translateYProperty(), getShape().getTranslateY()+deltaY)));
        setPosition(_center.add(new Point(deltaX, deltaY)));
    }

    /**
     * Allows control of the racket via keyboard interaction.
     * @param scene Scene object where the racket is to be played with
     */
    public abstract void setUpControls(Scene scene);

    public Shape getShape()
    {
        return _shape;
    }

    /**
     * Generates the racket's segments for collision detection.
     */
    public void generateSegments()
    {
        _segments.clear();
        Point p1 = new Point(_center.getX()-_width/2, _center.getY()-_height/2);
        Point p2 = new Point(_center.getX()+_width/2, _center.getY()-_height/2);
        Point p3 = new Point(_center.getX()+_width/2, _center.getY()+_height/2);
        Point p4 = new Point(_center.getX()-_width/2, _center.getY()+_height/2);
        addSegment(new Segment(p1,p2));
        addSegment(new Segment(p2,p3));
        addSegment(new Segment(p3,p4));
        addSegment(new Segment(p4,p1));
    }

    /**
     * Sets the racket position.
     * @param p the new position
     */
    public void setPosition(Point p)
    {
        _center = p;
        _shape.setX(_center.getX()-_width/2);
        _shape.setY(_center.getY()-_height/2);
        generateSegments();
    }

    /**
     * Getter.
     * @return the racket's timeline
     */
    public Timeline getTimeline() {
        return _timeline;
    }

    /**
     * Getter.
     * @return if the racket is IA controlled
     */
    public boolean isIA()
    {
        return _IA;
    }
}
