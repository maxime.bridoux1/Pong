package base.display.racket;

import data.Point;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Creates a human controlled racket.
 */
public class HumanRacket extends AbstractRacket
{
    public HumanRacket(Point center, double width, double height, double min, double max)
    {
        _IA = false;
        _center = center;
        _width = width;
        _height = height;
        setUpRacket();
    }

    /**
     * Moves the racket accordingly to the keyboard events when a key is pressed
     * @param event the keyboard event
     */
    public void actionOnKeyPressed(KeyEvent event)
    {
        if(event.getCode() == KeyCode.UP)
        {
            move(0, -10);
        }
        if(event.getCode() == KeyCode.DOWN)
        {
            move(0, 10);
        }
    }

    /**
     * Moves the racket accordingly to the keyboard events when a key is released
     * @param event the keyboard event
     */
    public void actionOnKeyReleased(KeyEvent event)
    {
        _timeline.play();
    }

    /**
     * Links the keyboard played on scene to the inner control fonction
     * @param scene Scene object where the racket is to be played with
     */
    @Override
    public void setUpControls(Scene scene)
    {
        scene.setOnKeyPressed(this::actionOnKeyPressed);
        scene.setOnKeyReleased(this::actionOnKeyReleased);
    }
}


