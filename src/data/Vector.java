package data;

/**
 * Represents a direction vector of two coordinates.
 */
public class Vector
{
    private double _dirX;
    private double _dirY;

    public Vector()
    {
        _dirX = 0;
        _dirY = 0;
    }

    public Vector(double dirX, double dirY)
    {
        _dirX = dirX;
        _dirY = dirY;
        normalise();
    }

    public double getDirX()
    {
        return _dirX;
    }

    public double getDirY()
    {
        return _dirY;
    }

    public void setDirX(double dirX)
    {
        _dirX = dirX;
        normalise();
    }
    public void setDirY(double dirY)
    {
        _dirY = dirY;
        normalise();
    }
    public void normalise()
    {
        if (_dirX != 0 || _dirY != 0)
        {
            double factor = Math.sqrt(Math.pow(_dirX, 2) + Math.pow(_dirY, 2));
            _dirX = _dirX/factor;
            _dirY = _dirY/factor;
        }
    }
}
